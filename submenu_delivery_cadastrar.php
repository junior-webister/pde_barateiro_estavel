<?php 
$tel = $_GET['tel'];

 ?>
 <SCRIPT LANGUAGE="JavaScript">
<!-- Begin
nextfield = "nome"; // nome do primeiro campo do site
netscape = "";
ver = navigator.appVersion; len = ver.length;
for(iln = 0; iln < len; iln++) if (ver.charAt(iln) == "(") break;
netscape = (ver.charAt(iln+1).toUpperCase() != "C");

function keyDown(DnEvents) {
// ve quando e o netscape ou IE
k = (netscape) ? DnEvents.which : window.event.keyCode;
if (k == 13) { // preciona tecla enter
if (nextfield == 'done') {
submit('document.form1');
return false;
//return true; // envia quando termina os campos
} else {
// se existem mais campos vai para o proximo
eval('document.form1.' + nextfield + '.focus()');
return false;
}
}
}

document.onkeydown = keyDown; // work together to analyze keystrokes
if (netscape) document.captureEvents(Event.KEYDOWN|Event.KEYUP);
// End -->
</script>

<!-- Adicionando Javascript -->
    <script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#rua").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#uf").val("");
                $("#ibge").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#uf").val("...");
                        $("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                                $("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>
<div class="cadastrar">
<!-- <div class="ui secondary pointing menu">
    <a class="item" href="#" onclick="location.reload()">
        Delivery
      </a>
        <a class="item active" href="#cadastrar">
        Cadastrar
      </a>
        <a class="item" href="pdv_delivery.php" onclick="location.reload()">
        Caixa
      </a>
</div> -->
<div class="ui segment">
    <h2 class="ui center aligned dividing header"><i class="ui user icon"></i>Cadastro de Clientes</h2>
    <br><br>
    <div class="ui center aligned grid">
        <form action="cadastrar_clientes.php" method="post" name="form1">
        <div class="ui form">
        <div class="fields">
          <div class="ten wide field">
            <label>Nome:</label>
            <input name="nome" type="text" id="nome" value="" maxlength="30" autofocus onFocus="nextfield ='cep';"/>
          </div>
          <div class="five wide field">
          <?php 
          // VERIFICA SE O TELEFONE JÁ FOI DIGITADO
            if ($tel == "") {
              echo '<label>Telefone:</label>
                    <input name="telefone" type="text" id="telefone" value="" size="15" maxlength="15"/>';
          } else {
                echo '<label>Telefone:</label>
                    <input name="telefone" type="text" id="telefone" placeholder="'.$tel.'" value="'.$tel.'" size="15" maxlength="15"/>';
          }
          ?>
            
          </div>
        </div>
            <div class="fields">
              <div class="five wide field">
                <label>Cep:</label>
                <input name="cep" type="text" id="cep" value="" size="10" maxlength="9" onFocus="nextfield ='cf1';"/>
              </div>
              <div class="two wide field">
                    <label>N°</label>
                    <input type="text" name="cf1" size="1" id="cf1" placeholder="" onFocus="nextfield ='taxa';">
              </div>
              <div class="three wide field">
                    <label>Taxa de Entrega</label>
                    <input type="text" name="taxa" placeholder="R$" size="4" onFocus="nextfield ='done';">
                </div>
              <div class="seven wide field">
                <label>Rua:</label>
                <input name="rua" type="text" id="rua" size="60" />
            </div>
              
            </div>
            <div class="fields">
            <div class="field">
                <label>Bairro:</label>
                <input name="bairro" type="text" id="bairro" size="40" />
              </div>
                <div class="seven wide field">
                    <label>Cidade:</label>
                    <input name="cidade" type="text" id="cidade" size="40" />
                </div>
                <div class="five wide field">
                  <label>Estado:</label>
                  <input name="uf" type="text" id="uf" size="2" />
                </div>
                

            </div>
        </div>
        <a href="#" target="_blank" class="ui button" onclick="this.href='consulta_rotas.php?rua='+document.getElementById('rua').value"><i class="map icon"></i>Mapa</a>
        <input type="submit" class="ui submit right floated blue button" value="Cadastrar">
    </form>
    </div> 
    <br>
             <div class='ui divider'>
             
         </div>
         <?php
            include 'ver_clientes.php';
                include 'popup_altera_cliente.php';
         ?>
    </div>
</div>
